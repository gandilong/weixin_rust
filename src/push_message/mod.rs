use crate::agent::Agent;
use crate::agent::tools;

pub struct MessageService{
    pub agent:Agent
}

impl MessageService {
    /*
    发送应用消息
    #url
    https://qyapi.weixin.qq.com/cgi-bin/message/send
     */
    ///发送应用消息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/message/send
    pub fn send(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    更新模版卡片消息
    #url
    https://qyapi.weixin.qq.com/cgi-bin/message/update_template_card
     */
    ///更新模版卡片消息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/message/update_template_card
    pub fn update_template_card(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/message/update_template_card?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    撤回应用消息
    #url
    https://qyapi.weixin.qq.com/cgi-bin/message/recall
     */
    ///撤回应用消息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/message/recall
    pub fn recall(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/message/recall?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    创建群聊会话
    #url
    https://qyapi.weixin.qq.com/cgi-bin/appchat/create
     */
    ///创建群聊会话
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/appchat/create
    pub fn chat_create(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/appchat/create?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
   更新群聊会话
   #url
   https://qyapi.weixin.qq.com/cgi-bin/appchat/update
  #参数结构
  ```
  {
    "chatid" : "CHATID",
    "name" : "NAME",
    "owner" : "userid2",
    "add_user_list" : ["userid1", "userid2", "userid3"],
    "del_user_list" : ["userid3", "userid4"]
}
  ```
    */
    ///更新群聊会话
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/appchat/update
    pub fn chat_update(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/appchat/update?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    获取群聊会话
    # url
https://qyapi.weixin.qq.com/cgi-bin/appchat/get
    #返回结果：```
     {
   "errcode" : 0,
   "errmsg" : "ok",
   "chat_info" : {
      "chatid" : "CHATID",
      "name" : "NAME",
      "owner" : "userid2",
      "userlist" : ["userid1", "userid2", "userid3"]
   }
 }
    ```
     */
    ///获取群聊会话
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/appchat/get
    pub fn get_chat(&mut self,chatid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/appchat/get?access_token={}&chatid={}",self.agent.get_access_token(),chatid);
        tools::get_json(url)
    }

    /*
      应用推送消息
      #url
      https://qyapi.weixin.qq.com/cgi-bin/appchat/send
     */
    ///应用推送消息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/appchat/send
    pub fn chat_send(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/appchat/send?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }


    /*
    互联企业消息推送
    发送应用消息
    #url
    https://qyapi.weixin.qq.com/cgi-bin/linkedcorp/message/send
     */
    ///互联企业消息推送,发送应用消息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/linkedcorp/message/send
    pub fn message_send(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/linkedcorp/message/send?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    发送「学校通知」
     */
    ///发送「学校通知」
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/message/send
    pub fn notice_send(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/message/send?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

}