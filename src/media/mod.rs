use crate::agent::Agent;
use crate::agent::tools;

pub struct MediaService{
    pub agent:Agent
}

impl MediaService {

    ///上传临时图片素材
    ///# 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=image
    pub fn upload_temp_image(&mut self,file_name:&str,file_bytes:Vec<u8>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=image",self.agent.get_access_token());
        tools::upload(url,String::from(file_name),file_bytes)
    }

    ///上传临时语音素材
    ///# 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=voice
    pub fn upload_temp_voice(&mut self,file_name:&str,file_bytes:Vec<u8>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=voice",self.agent.get_access_token());
        tools::upload(url,String::from(file_name),file_bytes)
    }

    ///上传临时视频素材
    ///# 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=video
    pub fn upload_temp_video(&mut self,file_name:&str,file_bytes:Vec<u8>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=video",self.agent.get_access_token());
        tools::upload(url,String::from(file_name),file_bytes)
    }

    ///上传临时文件素材
    ///# 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=file
    pub fn upload_temp_file(&mut self,file_name:&str,file_bytes:Vec<u8>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={}&type=file",self.agent.get_access_token());
        tools::upload(url,String::from(file_name),file_bytes)
    }

    ///上传图片
    ///# 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg
    pub fn upload_image(&mut self,file_name:&str,file_bytes:Vec<u8>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg?access_token={}",self.agent.get_access_token());
        tools::upload(url,String::from(file_name),file_bytes)
    }


}