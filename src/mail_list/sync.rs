use crate::agent::Agent;
use crate::agent::tools;

/*
   异步接口
 */
pub struct SyncService{
    pub agent:Agent
}

impl SyncService{
    /*
         syncuser
         增量更新成员
       */
    ///增量更新成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser
    pub fn syncuser(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         replaceuser
         全量覆盖成员
       */
    ///全量覆盖成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser
    pub fn replaceuser(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         replaceparty
         全量覆盖部门
       */
    ///全量覆盖部门
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty
    pub fn replaceparty(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         getresult
         获取异步任务结果
       */
    ///获取异步任务结果
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/batch/getresult
    pub fn get_sync_result(&mut self,jobid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/batch/getresult?access_token={}&jobid={}",self.agent.get_access_token(),jobid);
        tools::get_json(url)
    }


    /*
         simple_user
         导出成员
       */
    ///导出成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/export/simple_user
    pub fn simple_user(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/export/simple_user?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         export_user
         导出成员详情
       */
    ///导出成员详情
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/export/user
    pub fn export_user(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/export/user?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         export_department
         导出部门
       */
    ///导出部门
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/export/department
    pub fn export_department(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/export/department?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         export_taguser
         导出标签成员
       */
    ///导出标签成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/export/taguser
    pub fn export_taguser(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/export/taguser?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
         get_result
         获取导出结果
       */
    ///获取导出结果
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/export/get_result
    pub fn get_export_result(&mut self,jobid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/export/get_result?access_token={}&jobid={}",self.agent.get_access_token(),jobid);
        tools::get_json(url)
    }
}