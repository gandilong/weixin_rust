use crate::agent::Agent;
use crate::agent::tools;

/*
用户相关接口,不包括异步接口
 */
pub struct UserService{
    pub agent:Agent
}

impl UserService{

    /*
      create
      创建成员
    */
    ///创建成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/create
    pub fn create(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
      get
      读取成员
     */
    ///读取成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/get
    pub fn get_user(&mut self,userid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={}&userid={}",self.agent.get_access_token(),userid);
        tools::get_json(url)
    }

    /*
       update
       更新成员
     */
    ///更新成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/update
    pub fn update_user(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
      delete
      删除成员
     */
    ///删除成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/delete
    pub fn delete(&mut self,userid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={}&userid={}",self.agent.get_access_token(),userid);
        tools::get_json(url)
    }

    /*
       batchdelete
       批量删除成员
     */
    ///批量删除成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete
    pub fn batchdelete_user(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
       simplelist
       获取部门成员
     */
    ///获取部门成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/simplelist
    pub fn simplelist(&mut self,department_id:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token={}&department_id={}",self.agent.get_access_token(),department_id);
        tools::get_json(url)
    }


    /*
       list
       获取部门成员详情
     */
    ///获取部门成员详情
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/list
    pub fn user_list(&mut self,department_id:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={}&department_id={}",self.agent.get_access_token(),department_id);
        tools::get_json(url)
    }

    /*
       convert_to_openid
       userid与openid互换
     */
    ///convert_to_openid，      userid与openid互换
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid
    pub fn convert_to_openid(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
       authsucc
       二次验证
     */
    ///二次验证
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/authsucc
    pub fn authsucc(&mut self,userid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token={}&userid={}",self.agent.get_access_token(),userid);
        tools::get_json(url)
    }

    /*
       invite
       邀请成员
     */
    ///邀请成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/batch/invite
    pub fn invite(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/batch/invite?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
       get_join_qrcode
       获取加入企业二维码
     */
    ///获取加入企业二维码
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/corp/get_join_qrcode
    pub fn get_join_qrcode(&mut self,size_type:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/corp/get_join_qrcode?access_token={}&size_type={}",self.agent.get_access_token(),size_type);
        tools::get_json(url)
    }

    /*
       getuserid
      手机号获取userid
     */
    ///手机号获取userid
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/getuserid
    pub fn get_userid(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
       getuserid
      邮箱获取userid
     */
    ///邮箱获取userid
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/get_userid_by_email
    pub fn get_userid_by_email(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/get_userid_by_email?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
       list_id
      获取成员ID列表
     */
    ///获取成员ID列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/user/list_id
    pub fn list_id(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/user/list_id?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

}