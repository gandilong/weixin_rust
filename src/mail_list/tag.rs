use crate::agent::Agent;
use crate::agent::tools;
pub struct TagService{
    pub agent:Agent
}

impl TagService{
    /*
      create
      创建标签
    */
    ///创建标签
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/create
    pub fn create(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
      update
      更新标签名字
    */
    ///更新标签名字
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/update
    pub fn update(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
      delete
      删除标签
     */
    ///删除标签
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/delete
    pub fn delete(&mut self,tagid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token={}&tagid={}",self.agent.get_access_token(),tagid);
        tools::get_json(url)
    }

    /*
      get
      获取标签成员
     */
    ///获取标签成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/get
    pub fn get(&mut self,tagid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token={}&tagid={}",self.agent.get_access_token(),tagid);
        tools::get_json(url)
    }

    /*
      addtagusers
      创建标签
    */
    ///添加标签成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers
    pub fn add_tag_users(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
      deltagusers
      删除标签成员
    */
    ///删除标签成员
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers
    pub fn delete_tag_users(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
     list
     获取标签列表
    */
    ///获取标签列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/tag/list
    pub fn list(&mut self)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token={}",self.agent.get_access_token());
        tools::get_json(url)
    }
}