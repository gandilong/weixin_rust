use crate::agent::Agent;
use crate::agent::tools;
pub struct DeptService{
    pub agent:Agent
}

impl DeptService{


    /*
      create
      创建部门
    */
    ///创建部门
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/create
    pub fn create(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
          update
          更新部门
        */
    ///更新部门
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/update
    pub fn update(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }


    /*
      delete
      删除部门
     */
    ///删除部门
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/delete
    pub fn delete(&mut self,deptid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token={}&id={}",self.agent.get_access_token(),deptid);
        tools::get_json(url)
    }

    /*
      list
      获取部门列表
     */
    ///获取部门列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/list
    pub fn list(&mut self,deptid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={}&id={}",self.agent.get_access_token(),deptid);
        tools::get_json(url)
    }

    /*
      simplelist
      获取子部门ID列表
     */
    ///获取子部门ID列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/simplelist
    pub fn simplelist(&mut self,deptid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/simplelist?access_token={}&id={}",self.agent.get_access_token(),deptid);
        tools::get_json(url)
    }

    /*
      get
      获取单个部门详情
     */
    ///获取单个部门详情
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/department/get
    pub fn get(&mut self,deptid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/department/get?access_token={}&id={}",self.agent.get_access_token(),deptid);
        tools::get_json(url)
    }
}