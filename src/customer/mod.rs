use crate::agent::Agent;
use crate::agent::tools;

pub struct CustomerService{
    pub agent:Agent
}

impl CustomerService {
    /*
    获取配置了客户联系功能的成员列表
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_follow_user_list
     */
    ///获取配置了客户联系功能的成员列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_follow_user_list
    pub fn get_follow_user_list(&mut self)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_follow_user_list?access_token={}",self.agent.get_access_token());
        tools::get_json(url)
    }

    /*
    获取客户列表
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list
     */
    ///获取客户列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list
    pub fn list(&mut self,userid:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list?access_token={}&userid={}",self.agent.get_access_token(),userid);
        tools::get_json(url)
    }

    /*
   获取客户详情
   #url
   https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get
    */
    ///获取客户详情
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get
    pub fn get(&mut self,external_userid:String,cursor:Option<String>)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=match cursor {
            Some(cur)=>format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token={}&external_userid={}&cursor={}",self.agent.get_access_token(),external_userid,cur),
            None=>format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token={}&external_userid={}",self.agent.get_access_token(),external_userid),
        };
        tools::get_json(url)
    }

    /*
    批量获取客户详情
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user
     */
    ///批量获取客户详情
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user
    pub fn get_by_user(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    修改客户备注信息
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark
     */
    ///修改客户备注信息
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark
    pub fn remark(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    获取规则组列表
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/list
     */
    ///获取规则组列表
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/list
    pub fn list_strategy(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/list?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    获取企业标签库
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list
     */
    ///获取企业标签库
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list
    pub fn get_corp_tag_list(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    获取指定规则组下的企业客户标签
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_strategy_tag_list
     */
    ///获取指定规则组下的企业客户标签
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_strategy_tag_list
    pub fn get_strategy_tag_list(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_strategy_tag_list?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }

    /*
    编辑客户企业标签
    #url
    https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag
     */
    ///编辑客户企业标签
    /// # 微信接口
    /// https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag
    pub fn mark_tag(&mut self,json_params:&str)->Result<serde_json::Value, reqwest::Error>{
        self.agent.fresh_token();
        let url=format!("https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token={}",self.agent.get_access_token());
        tools::post_json(url,Some(String::from(json_params)))
    }
}