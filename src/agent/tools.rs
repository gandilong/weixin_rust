use std::time::{SystemTime, UNIX_EPOCH};
use reqwest::Error;
use std::borrow::Cow;
//use serde_json::{json, Value};
///获取当前的秒数
pub fn get_current_second()->u64{
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");

    return since_the_epoch.as_secs();
}
///根据地址发get请求
pub fn get(url:String)->Result<String,Error>{
    reqwest::blocking::get(url)?.text_with_charset("UTF-8")
}

///根据地址发get请求,并获取json结果
pub fn get_json(url:String)->Result<serde_json::Value,Error>{
    reqwest::blocking::get(url)?.json()
}

///根据地址发post请求
pub fn post(url:String,params:String)->Result<String,Error>{
    reqwest::blocking::Client::new().post(url).body(params).send()?.text_with_charset("UTF-8")
}

///根据地址发post请求,获取json对角
/// 参数为Option<String>，可选
/// # 例子
///```
/// use weixin_rust::agent::tools;
/// use weixin_rust::tools;
///
/// tools::post_json("http://....".to_string(),None);
///
/// tools::post_json("http://....".to_string(),Some("".to_string()));
///
/// ```
pub fn post_json(url:String,params:Option<String>)->Result<serde_json::Value,Error>{
    match params{
        Some(param)=>reqwest::blocking::Client::new().post(url).body(param).send()?.json(),
        None=>reqwest::blocking::Client::new().post(url).body("").send()?.json()
    }
}

pub fn upload(url:String,file_name:String,bytes:Vec<u8>)->Result<serde_json::Value,Error>{
    let part=reqwest::blocking::multipart::Part::bytes(Cow::from(bytes)).file_name(Cow::from(file_name.clone()));
    let form = reqwest::blocking::multipart::Form::new().text("filename",file_name).text("name","media").part("file",part);
    reqwest::blocking::Client::new().post(url).multipart(form).send()?.json()
}



