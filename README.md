#这是一个三方工具包，用于调用企业微信的接口
# 工具方法约定

1. 基本上大部分接口传参都是json字符串，少数接口参数比较简单，直接传。

2. 返回数据都是Result<serde_json::Value, reqwest::Error>类型，这个类型的使用方使可以参靠serde_json

3. 由于access_token是在内部维护更新的，所以agent变量必须是可变的mut

4. 目前可用的接口比较少，会陆续更新上，敬请期待...

5. git地址：https://gitee.com/gandilong/weixin_rust.git

6. 更新详细文档地址：https://gitee.com/gandilong/weixin_rust/wikis/Home

7. 可能通过weixin_rust中的工具方法自己实现接口调用，例如

8. ```rust
   use agent::tools;
   
   pub fn some_weixin_interface(agent:&mut Agent,son_params:String){
       agent.fresh_token();
           let url=format!("https://qyapi.weixin.qq.com/cgi-bin/***?access_token={}",agent.get_access_token());
           tools::post_json(url,json_params)
   }
   
   pub fn some_weixin_interface(agent:&mut Agent,id:String){
       agent.fresh_token();
           let url=format!("https://qyapi.weixin.qq.com/cgi-bin/***?access_token={}&id={}",agent.get_access_token(),id);
           tools::get_json(url)
   }
   
   
   ```



# 开始使用

```rust
use weixin_rust::agent;//引入应用模块
fn main(){
    //由于access_token是内部自动维护的，需要定时更新，所以ag必须是mut 可变的
    let mut ag= agent::Agent::new("corpid".to_string(),"agent_secret".to_string());
    ag.fresh_token();//这个方法一般情况下是不需要手动调用的。只有在刚new出来的agent的情况下才需要。
    println!("token={}",ag.get_access_token());//这个方法额外提供的，返回的是&str
}
```

### 获取可信IP

```rust
use weixin_rust::agent;

fn main(){
    let mut ag= agent::Agent::new("corpid".to_string(),"agent_secret".to_string());
    let r=ag.get_api_domain_ip();//所有接口方法内部会自动刷新access_token
    println!("{}",r["ip_list"]);
    //注意：所有接口返回的类型都是：Result<serde_json::Value, reqwest::Error>
}
```



# open_userid转明文userid

```rust
use weixin_rust::agent;
fn main(){
    let mut ag= agent::Agent::new("corpid".to_string(),"agent_secret".to_string());
    let params_json=json!({
      "open_userid_list":["xxx", "yyy"],
      "source_agentid":100001
    });

    //也可以把一个struct转成json字符串
    let params_json_str=serde_json::to_string(&params_json).unwrap();
    let r=ag.openuserid_to_userid(params_json_str);
}
```

# 通讯录

## 用户功能

```rust
use weixin_rust::agent;
use weixin_rust::mail_list;

fn main(){
    let mut ag= agent::Agent::new("corpid".to_string(),"agent_secret".to_string());
    let mut user_service=ag.get_user_service();//获取用户服务对象
    let ur=user_service.get_user(String::from("123456"));
    println!("{:?}",ur);
}
```



